﻿using System;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            //Count determines how many items we want in our array
            var count = 10;

            //Make a variable called rnd which makes a random number
            Random rnd = new Random();

            //Array that stores random number
            var array = new int[count];
            
            //for loop which adds random number between 1 and 10 and adds to the array
            for (var i = 0; i < count; i++) {

                array[i] = rnd.Next(1, 10);
            }

            //Displays the array back to the user e.g. 0,1,2,3,4,5,6,7,8,9
            Console.WriteLine(string.Join(',', array));
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
            
        }
    }
}
